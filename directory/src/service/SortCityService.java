package service;

import models.City;

import java.text.MessageFormat;
import java.util.*;

public class SortCityService {

    /*Сортировка списка городов по федеральному округу и наименованию города внутри каждого федерального
     округа в алфавитном порядке по убыванию с учетом регистра;*/
    public List<City> sortByDistrictAndName(List<City> cityList) {

        cityList.sort(Comparator.comparing(City::getDistrict).reversed().thenComparing(City::getName));

        return cityList;
    }

    //Сортировка списка городов по наименованию в алфавитном порядке по убыванию без учета регистра;
    public List<City> sortByNameWithoutRegister(List<City> cityList) {

        cityList.sort(Comparator.comparing(City::getName, String.CASE_INSENSITIVE_ORDER));

        return cityList;
    }

    //Сортировка списка городов по наименованию в алфавитном порядке по убыванию без учета регистра через lambda;
    public List<City> sortByNameWithoutRegisterLambda(List<City> cityList) {

        cityList.sort((c1, c2) -> c1.getName().compareToIgnoreCase(c2.getName()));

        return cityList;
    }

    //Вывод самого населенного города;
    public String topPopulationCity(List<City> cityList) {

        City city = cityList.stream()
                .max(Comparator.comparing(City::getPopulation))
                .get();

        return "[" + city.getId() + "]" + " = " + city.getPopulation();
    }

    //Вывод количества городов в разрезе регионов.
    public void countCitiesInRegion(List<City> cityList) {

        Map<String, Integer> countCitiesInRegion = new HashMap<>();

        for (City city : cityList) {
            if (!countCitiesInRegion.containsKey(city.getRegion())) {
                countCitiesInRegion.put(city.getRegion(), 1);
            }

            countCitiesInRegion.put(city.getRegion(),
                    countCitiesInRegion.get(city.getRegion()) + 1);
        }

        for (Map.Entry<String, Integer> m : countCitiesInRegion.entrySet()) {
            System.out.println(m.getKey() + " - " + m.getValue());
        }
    }

    //Вывод количества городов в разрезе регионов вариант через lambda.
    public void findCountCityByRegionV2(List<City> cities) {
        Map<String, Integer> regions = new HashMap<>();
        cities.forEach(city -> regions.merge(city.getRegion(), 1, Integer::sum));
        regions.forEach((k, v) -> System.out.println(MessageFormat.format(" {0} = {1}", k, v)));
    }
}
