package service;

import models.City;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class CityParserService {
    Path filePath = Paths.get("directory/resources/city_ru.csv");

    //Парсинг стринги в объект.
    public City cityParser(String[] parts) {

        Long id = Long.valueOf(parts[0]);
        String name = parts[1];
        String region = parts[2];
        String district = parts[3];
        Long population = Long.valueOf(parts[4]);
        String foundation = parts[5];

        return new City(id, name, region, district, population, foundation);
    }


    //Печать листа с городами в консоль.
    public List<City> listCities() {
        List<City> cities = new ArrayList<>();

        try (Scanner scanner = new Scanner(new File(String.valueOf(filePath)))) {
            while (scanner.hasNextLine()) {
                //Разбивание строки на паттерны для класса.
                String[] parts = scanner.nextLine().split(";");
                //Проверка соответствия полей.
                if (parts.length < City.class.getDeclaredFields().length ||
                        Arrays.stream(parts).anyMatch(String::isEmpty)) {
                    continue;
                }
                //Парсинг города
                City newCity = cityParser(parts);

                if (newCity == null) {
                    System.out.println("log.error(City parse error...)");
                    continue;
                }

                cities.add(newCity);
            }
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        return cities;
    }
    public void print(List<City> cities) {
        cities.forEach(System.out::println);
    }





}