
import models.City;
import service.CityParserService;
import service.SortCityService;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        CityParserService cityParserService = new CityParserService();
        SortCityService sortCityService = new SortCityService();

        List<City> cityList = cityParserService.listCities();

        sortCityService.countCitiesInRegion(cityList);
//        sortCityService.findCountCityByRegionV2(cityList);
    }

}
